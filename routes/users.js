var express = require('express');
var router = express.Router();

var User = require('../models/User.js');
var bodyParser = require('body-parser');

var jsonParser = bodyParser.json()

/* GET users listing. */
router.get('/', jsonParser, function(req, res, next) {

  console.log(req.query.email);
  console.log(req.query.password);

  user = User.findOne({
    'email': req.query.email,
    'password': req.query.password
  },
      null,
      {
        safe: true
      },
      function (err, user) {
    if (err) return next(err);
    if(user){
      console.log('------------------------------');
      console.log('Found User');
      console.log('------------------------------');
      res.json(user);

    }else{
      next(new Error('User was not found.'));
    }

  });
});

/* GET /todos/id */
router.get('/:id', function(req, res, next) {
  User.findById(req.params.id, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

/* POST /todos */

router.post('/', jsonParser, function(req, res, next) {
  User.create(req.body, function (err, post) {
    if (err) return next(err);
    res.json(post);
    console.log('------------------------------');
    console.log('Create Works');
    console.log('------------------------------');
  });
});

/* PUT /todos/:id */
router.put('/:id', function(req, res, next) {
  User.findByIdAndUpdate(req.params.id, req.body, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

/* DELETE /todos/:id */
router.delete('/:id', function(req, res, next) {
  User.findByIdAndRemove(req.params.id, req.body, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});



module.exports = router;
